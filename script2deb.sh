#!/bin/sh
fpm -n fracatux \
-s dir \
-t deb \
--name fracatux \
--license GPL \
--version 1.5.2 \
--architecture all \
--description "Fracatux est un logiciel éducatif permettant de manipuler les fractions sous forme de barres." \
--url "https://forge.apps.education.fr/educajou/tuxblocs]https://forge.apps.education.fr/educajou/fracatux" \
--maintainer "Arnaud Champollion" \
linux2/fracatux.desktop=/usr/share/applications/fracatux.desktop \
assets/images/icone.png=/usr/share/pixmaps/fracatux.png \
linux2/fracatux=/usr/bin/fracatux \
dist/fracatux=/usr/share
